<?php
function sse_core_form_system_theme_settings_alter(&$form, $form_state) {
  $form['twitter_username'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Twitter username'),
    '#default_value' => theme_get_setting('twitter_username'),
    '#description'   => t("Enter your Twitter username (don't include '@')."),
  );
  $form['facebook_url'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Facebook URL'),
    '#default_value' => theme_get_setting('facebook_url'),
    '#description'   => t("Enter the URL to your Facebook page."),
  );
  $form['instagram_url'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Instagram URL'),
    '#default_value' => theme_get_setting('instagram_url'),
    '#description'   => t("Enter the URL to your Facebook page."),
  );
  $form['youtube_url'] = array(
    '#type'          => 'textfield',
    '#title'         => t('YouTube URL'),
    '#default_value' => theme_get_setting('youtube_url'),
    '#description'   => t("Enter the URL to your YouTube page."),
  );
}
