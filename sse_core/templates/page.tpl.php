<?php
//kpr(get_defined_vars());
//kpr($theme_hook_suggestions);
//template naming
//page--[CONTENT TYPE].tpl.php
?>
<?php if( theme_get_setting('mothership_poorthemers_helper') ){ ?>
<!-- page.tpl.php-->
<?php } ?>
<?php print $mothership_poorthemers_helper; ?>
<?php include_once("/var/www/websites/humber.ca_V2/_gv2/navigation.php"); ?>
<?php include_once("/var/www/websites/humber.ca_V2/_gv2/header.php"); ?>
<div class="website">
<header class="site-header" role="banner">
  <div class="container">
    <div class="brand">
      <h1>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" style="background:url('<?php print $logo; ?>') no-repeat left center;">
          <?php print $site_name; ?>
        </a>
      </h1>
    </div>
    <div id="top-menu">
      <?php
        $block = block_load('menu_block', '1');
        $blockContent = _block_get_renderable_array(_block_render_blocks(array($block)));
        $output = drupal_render($blockContent);
        print $output;
      ?>
    </div>
    <?php if($page['header']): ?>
      <div class="header-region">
        <?php print render($page['header']); ?>
      </div>
    <?php endif; ?>
  </div>
</header>

<div class="page">

  <div role="main" id="main-content">
    <header class="page-header" style="background-image:url(<?php print $headerImage; ?>);">
      <div class="overlay">
        <div class="container">
          <?php print render($title_prefix); ?>
          <?php if ($title): ?>
            <h1 class="page-title"><?php print $title; ?></h1>
          <?php endif; ?>
          <?php print render($title_suffix); ?>

          <?php //print $breadcrumb; ?>

          <?php if ($action_links): ?>
            <ul class="action-links"><?php print render($action_links); ?></ul>
          <?php endif; ?>

          <?php if (isset($tabs['#primary'][0]) || isset($tabs['#secondary'][0])): ?>
            <nav class="tabs"><?php print render($tabs); ?></nav>
          <?php endif; ?>

          <?php if($page['highlighted'] OR $messages){ ?>
            <div class="drupal-messages">
            <?php print render($page['highlighted']); ?>
            <?php print $messages; ?>
            </div>
          <?php } ?>
        </div>
      </div>
    </header>
    <div class="wgy-icon">
      <a href="//www.humber.ca/student-life">We Got You</a>
    </div>
    <section class="page-content">
      <div class="container">
        <?php print render($page['content_pre']); ?>
        <?php print render($page['content']); ?>
        <?php print render($page['content_post']); ?>
        <?php if ($page['sidebar_first']): ?>
        <aside class="sidebar-first">
          <?php print render($page['sidebar_first']); ?>
        </aside>
        <?php endif; ?>

        <?php if ($page['sidebar_second']): ?>
        <div class="sidebar-second">
          <?php print render($page['sidebar_second']); ?>
        </div>
        <?php endif; ?>
      </div>
    </section>

  </div><!-- /main-->

</div><!-- /page-->

<footer class="site-footer">
  <div class="container">
    <section class="social">
      <h1>Join Us On</h1>
      <ul>
        <li class="facebook-icon"><a href="<?php print theme_get_setting('facebook_url'); ?>">Facebook</a><li>
        <li class="twitter-icon"><a href="<?php print theme_get_setting('twitter_username'); ?>">Twitter</a><li>
        <li class="instagram-icon"><a href="<?php print theme_get_setting('instagram_url'); ?>">Instagram</a><li>
        <li class="youtube-icon"><a href="<?php print theme_get_setting('youtube_url'); ?>">YouTube</a><li>
      </ul>
    </section>

    <section class="social-widgets">
      <?php $ownTwitter = print_r (theme_get_setting('twitter_username'), true); ?>
      <?php
        if ($ownTwitter == 'lifeathumber') { ?>
          <div class="twitter-widget solo">
            <a class="twitter-timeline" href="https://twitter.com/<?php print theme_get_setting('twitter_username'); ?>" data-tweet-limit="1" data-cards="hidden">Tweets by <?php print theme_get_setting('twitter_username'); ?></a>
    <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
          </div>
      <?php } else { ?>
        <div class="twitter-widget">
          <a class="twitter-timeline" href="https://twitter.com/<?php print theme_get_setting('twitter_username'); ?>" data-tweet-limit="1">Tweets by <?php print theme_get_setting('twitter_username'); ?></a>
  <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
        </div>
        <div class="twitter-widget">
          <a class="twitter-timeline" href="https://twitter.com/lifeathumber" data-tweet-limit="1">Tweets by <?php print theme_get_setting('twitter_username'); ?></a>
  <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
        </div>
      <?php } ?>
    </section>
  </div>
</footer>
</div>
<!-- <footer role="contentinfo"> -->
  <?php //print render($page['footer']); ?>
<!-- </footer> -->
<?php include_once("/var/www/websites/humber.ca_V2/_gv2/footer.php"); ?>
