<?php
//kpr(get_defined_vars());
//http://drupalcontrib.org/api/drupal/drupal--modules--node--node.tpl.php
//node--[CONTENT TYPE].tpl.php

//$content['field_name']['#theme'] = "nomarkup";
//hide($content['field_name']);
if ($classes) {
  $classes = ' class="'. $classes . ' "';
}

if ($id_node) {
  $id_node = ' id="'. $id_node . '"';
}

hide($content['comments']);
hide($content['links']);
?>

<!-- node.tpl.php -->
<div class="carousel-cell" <?php print $id_node . $classes .  $attributes; ?> role="banner" style="background-image:url(<?php print render($content['field_img_banner_sse']);?>);">
  <?php print $mothership_poorthemers_helper; ?>
  <div class="overlay">
    <div class="wrapper">
      <div class="container">
          <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
        <?php if ($display_submitted): ?>
        <footer>
          <?php print $user_picture; ?>
          <span class="author"><?php print t('Written by'); ?> <?php print $name; ?></span>
          <span class="date"><?php print t('On the'); ?> <time><?php print $date; ?></time></span>

          <?php if(module_exists('comment')): ?>
            <span class="comments"><?php print $comment_count; ?> Comments</span>
          <?php endif; ?>
        </footer>
        <?php endif; ?>

        <?php print render($content['field_teaser_banner_sse']);?>

        <?php print render($content['field_link_banner_sse']);?>

        <?php print render($content['links']); ?>

        <?php print render($content['comments']); ?>
      </div>
    </div>
  </div>
</div>
