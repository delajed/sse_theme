<?php
//kpr(get_defined_vars());
//http://drupalcontrib.org/api/drupal/drupal--modules--node--node.tpl.php
//node--[CONTENT TYPE].tpl.php

//$content['field_name']['#theme'] = "nomarkup";
//hide($content['field_name']);
if ($classes) {
  $classes = ' class="'. $classes . ' "';
}

if ($id_node) {
  $id_node = ' id="'. $id_node . '"';
}

hide($content['comments']);
hide($content['links']);
?>

<!-- node.tpl.php -->
<div class="video-background" <?php print $id_node . $classes .  $attributes; ?> role="banner" style="background-image:url(<?php print render($content['field_img_banner_sse']);?>);">
  <?php print $mothership_poorthemers_helper; ?>
  <div class="media">
    <!-- <button type="button" onclick="vid_play_pause()">Play / Pause</button> -->
    <div class="background-container">
        <video id="background" preload="auto" autoplay="true" loop="loop" muted="muted" volume="0">
          <source src="<?php print render($content['field_video_webm']); ?>">
          <source src="<?php print render($content['field_video_mp4']); ?>">
        </video>
    </div>
  </div>
  <div class="wrapper">
    <div class="container">
      <?php print render($title_prefix); ?>
        <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
      <?php print render($title_suffix); ?>

      <?php print render($content['field_teaser_banner_sse']);?>

      <?php print render($content['field_link_banner_sse']);?>

    </div>
  </div>
</div>
