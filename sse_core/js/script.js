(function ($) {

  $('#top-menu').slicknav({
    label: '',
    prependTo:'.website'
  });

  $('.landing-page-carousel').flickity({
    // options
    cellAlign: 'left',
    contain: true,
    pageDots: false
  });

  $('.node-block-sse .overlay').matchHeight();

})(jQuery);
